# Table de La Baston

## Déscription

Tables utilisée par la baston

C'est un sous modules des [régles](https://gitlab.com/Ultraxime/de-bastonis-regulis.git).


## License

Les régles sont sous License Creative Commons Attribution-ShareAlike 4.0, consultable dans [LICENSE](LICENSE).
