# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-26 18:06:40
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-02-25 18:00:22

# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2022-11-09 17:14:14
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-12 16:59:08

# This file is part of Baston Editor.

# Baston Editor is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.

# Baston Editor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Editor. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing all the method regarding the use of LaTeX
"""

from io import StringIO
from os.path import dirname, join, realpath
from typing import Dict, List, Union

from pdf2image import convert_from_path
from pylatex import Command, Description, Document, Itemize, NoEscape, Package
from ruamel.yaml import YAML


def load_file(filename: str):
    """
    Loads a file.

    :param      filename:  The filename
    :type       filename:  str

    :returns:   The content of the file
    :rtype:     Any
    """
    yaml = YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    yaml.width = 80  # type: ignore
    yaml.explicit_start = True  # type: ignore
    yaml.version = (1, 2)  # type: ignore

    filename = join(dirname(dirname(realpath(__file__))), filename)
    with open(filename, "r", encoding="utf-8") as file:
        content = yaml.load(file)
    with open(filename, "w", encoding="utf-8") as file:
        yaml.dump(content, file)
    return content


latex = load_file("src/latex.yml")


def to_no_escape(string) -> NoEscape:
    """
    Turns a string to a NoEscape string

    :param      string:  The string
    :type       string:  Any

    :returns:   The correct noescape string.
    :rtype:     NoEscape
    """
    if not isinstance(string, str):
        string = str(string)

    i = 0
    while i < len(string):
        if string[i] == "_":
            string = string[:i] + "\\" + string[i:]
            i += 2
        elif string[i] == "\\" and i+1 < len(string) and string[i + 1] == "'":
            string = string[:i] + string[i + 1 :]
        elif string[i] == "«":
            string = string[:i] + "``" + string[i + 1 :]
            i += 2
        elif string[i] == "»":
            string = string[:i] + "''" + string[i + 1 :]
            i += 2
        else:
            ensuremath = False
            for word in latex["ensuremath"]:
                if i + len(word) < len(string):
                    match = True
                    for j, letter in enumerate(word):
                        if letter != string[i + j]:
                            match = False
                    if match:
                        string = (
                            string[:i]
                            + "\\ensuremath{"
                            + word
                            + "}"
                            + string[i + len(word) :]
                        )
                        i += len(word) + 13
                        ensuremath = True
            if not ensuremath:
                i += 1
    return NoEscape(string)


def convert_dict(
    dic: Dict, newline: bool = True
) -> Union[Description, Itemize, NoEscape]:
    """
    Convert a dictionnary to a latex description

    :param      dic:      The dic
    :type       dic:      Dict
    :param      newline:  The newline
    :type       newline:  bool

    :returns:   The pylatex object representing in latex the dict
    :rtype:     Description or Itemize or NoEscape
    """

    if len(dic) == 1:
        newline = False
        for k, value in dic.items():
            if to_no_escape(k) != "description":
                key = NoEscape(to_no_escape(k) + " :")
            else:
                key = ""

            if key == "":
                return convert(value, newline)
            desc = Description(
                options=NoEscape(
                    "style=unboxed,"
                    + "leftmargin=0.5\\parindent,"
                    + "format=\\normalfont"
                )
            )
            value = convert(value, newline)
            if isinstance(value, Itemize):
                desc.append(Command("pagebreak", options="2"))
                if key != "":
                    desc.add_item("", "")
                desc.append(Command("nopagebreak", options="4"))
                desc.add_item(key, value)
            else:
                desc.add_item(key, value)
            return desc
    desc = Description(
        options=NoEscape(
            ("style=nextline," if newline else "style=unboxed,")
            + "leftmargin=0.5\\parindent,"
            + "format=\\normalfont"
        )
    )
    newline = False

    for k, value in dic.items():
        if to_no_escape(k) != "description":
            key = NoEscape(to_no_escape(k) + " :")
        else:
            key = ""

        value = convert(value, newline)
        if isinstance(value, Itemize):
            desc.append(Command("pagebreak", options="2"))
            # if key != "":
            #     desc.add_item("", "")
            desc.append(Command("nopagebreak", options="4"))
            desc.add_item(key, value)
        else:
            desc.add_item(key, value)
    return desc

def print_dict(dic: dict, newline: bool = True):
    """
    Prints a dictionary.

    :param      dic:      The dic
    :type       dic:      dict
    :param      newline:  The newline
    :type       newline:  bool
    """
    print(convert_dict(dic, newline).dumps().replace('%',''))

def create_doc_preamble(preview: bool = False) -> Document:
    """
    Creates a document.

    :returns:   A pylatex document
    :rtype:     Document
    """
    if preview:
        doc = Document(
            documentclass="article",
            document_options="10pt",
            geometry_options={
                "paperwidth": "8cm",
                "margin": "0cm",
                "paperheight": "3cm",
            },
        )
        doc.packages.append(Package("preview", options="active, tightpage"))
        doc.packages.append(Package("etoolbox"))
        parident = NoEscape(
            "\\edef\\keptparindent{\\the\\parindent}\n"
            + "\\patchcmd{\\preview}\n"
            + "{\\ignorespaces}\n"
            + "{\\parindent\\keptparindent\\ignorespaces}\n"
            + "{}{}"
        )
        doc.preamble.append(parident)
        doc.preamble.append(NoEscape("\\setlength{\\PreviewBorder}{0.5cm}"))
    else:
        doc = Document(documentclass="article",
                       document_options="10pt, twocolumn")

    doc.packages.append(Package("babel", options="french"))

    for package in latex["packages"]:
        doc.packages.append(Package(package))

    for command in latex["preamble"]:
        doc.preamble.append(to_no_escape(command))

    return doc


def create_preview(content: str, newline: bool = True):
    """
    Creates a preview.

    :param      content:  The content
    :type       content:  str
    :param      newline:  The newline
    :type       newline:  bool
    """
    doc = create_doc(content, newline, preview=True)
    doc.generate_pdf("tmp", compiler="xelatex")
    pages = convert_from_path("tmp.pdf")
    pages[0].save("tmp.png", "PNG")


def create_doc(content, newline: bool = True,
               preview: bool = False) -> Document:
    """
    Creates a document.

    :param      content:  The content
    :type       content:  Any
    :param      newline:  The newline
    :type       newline:  bool
    :param      preview:  The preview
    :type       preview:  bool

    :returns:   Return the document representing the content
    :rtype:     Document
    """
    doc = create_doc_preamble(preview=preview)

    if preview:
        doc.append(NoEscape("\\begin{preview}"))
    doc.append(convert(content, newline))
    if preview:
        doc.append(NoEscape("\\end{preview}"))

    return doc


def convert_list(
    liste: List, newline: bool = True
) -> Union[NoEscape, Itemize, Description]:
    """
    Convert a list to a latex Itemize

    :param      liste:    The liste
    :type       liste:    List
    :param      newline:  The newline
    :type       newline:  bool

    :returns:   The pylatex object representing in latex the list
    :rtype:     Itemize or Description or NoEscape
    """
    if len(liste) == 1:
        value = liste[0]
        return convert(liste[0], newline)

    itemize = Itemize(
        options=NoEscape("label={}," + "noitemsep," + "leftmargin=0\\parindent")
    )
    for value in liste:
        itemize.add_item(convert(value, newline))
    return itemize


def convert(value, newline: bool = False) -> Union[NoEscape,
                                                   Itemize,
                                                   Description]:
    """
    Convert a a python var to a latex representation

    :param      value:    The value
    :type       value:    Any
    :param      newline:  The newline
    :type       newline:  bool

    :returns:   A pylatex object reprensenting the python object
    :rtype:     NoEscape | Itemize | Description
    """
    if isinstance(value, dict):
        return convert_dict(value, newline)
    if isinstance(value, list):
        return convert_list(value, newline)
    return to_no_escape(value)


def to_yaml(value: str):
    """
    Turn a yaml string into somethin understanble by python

    :param      value:  The value
    :type       value:  str

    :returns:   The python representation of the yaml string
    :rtype:     Any
    """
    yaml = YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    yaml.width = 80  # type: ignore
    content = yaml.load(value)
    return content


def to_string(value) -> str:
    """
    Convert the value to a yaml string representation

    :param      value:  The value
    :type       value:  Any yamlable

    :returns:   the yaml sting
    :rtype:     str
    """
    yaml = YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    yaml.width = 80  # type: ignore
    yaml.explicit_start = False  # type: ignore

    stream = StringIO()
    yaml.dump(value, stream)
    output = stream.getvalue()
    stream.close()
    return output

powers = None

def load_power() -> dict:
    jean_power = dict(load_file("petits_numeros.yml"))
    george_power = dict(load_file("petits_numeros_negatifs.yml"))
    our_power = dict(load_file("petit_num.yml"))

    dic = jean_power
    dic.update(george_power)
    dic.update(our_power)

    return dic

def get_power(name: str) -> dict:
    """
    Gets the power.

    :param      name:  The name
    :type       name:  str

    :returns:   The power representation in yaml.
    :rtype:     str
    """
    if powers is None:
        powers = load_power()

    try:
        power = int(name)
    except ValueError:
        power = name

    try:
        content = {power: jean_power[power]}
    except KeyError:
        try:
            content = {power: george_power[power]}
        except KeyError:
            try:
                content = {power: our_power[power]}
            except KeyError:
                content = {power: {"description": ""}}

    return content
